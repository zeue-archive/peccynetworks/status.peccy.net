FROM ubuntu:18.10

ENV HUGO_VERSION=0.50
ENV HUGO_TYPE=_extended

RUN apt-get update && apt-get install npm -y

ENV HUGO_ID=hugo${HUGO_TYPE}_${HUGO_VERSION}
ADD https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/${HUGO_ID}_Linux-64bit.tar.gz /tmp
RUN tar -xf /tmp/${HUGO_ID}_Linux-64bit.tar.gz -C /tmp \
    && mkdir -p /usr/local/sbin \
    && mv /tmp/hugo /usr/local/sbin/hugo \
    && rm -rf /tmp/${HUGO_ID}_linux_amd64 \
    && rm -rf /tmp/${HUGO_ID}_Linux-64bit.tar.gz \
    && rm -rf /tmp/LICENSE.md \
    && rm -rf /tmp/README.md

RUN mkdir status.peccy.net

COPY . ./status.peccy.net

RUN cd status.peccy.net && npm i && hugo

EXPOSE 80/tcp

CMD status.peccy.net/node_modules/.bin/serve -l 80 status.peccy.net/public
